# Examen ELK 01/07/2020

Consigne de rendu:

Mettre tout les rendus dans une archive nommée de la façon suivante: `<NOM>_<Prenom>_exam_ELK_01-07-2020`
Dans cette archive:
- Un dossier par exercice
- Dans chaque dossier: Les fichiers de rendu de l'exercice.

Envoyer cette archive par mail à gweltaz.gestin@wijin.tech avec comme objet \<NOM\> \<Prenom\> Rendu Exam ELK 01-07-2020

## QCM (5 points)

[QCM](https://forms.gle/dyi8MjGgK8RD9QcX8)

## Exercice 1 (6 points)
Injection de données depuis une api dans Elasticsearch via logstash

### Consigne
1. Configurer Logstash pour injecter périodiquement des utilisateurs via l'url suivante `https://randomuser.me/api/` 
2. Modifier les données pour ne garder que les données du tableau `results` 
3. Supprimer les objets `id` et `picture`
4. Mettre la valeur de la propriété `nat` dans l'index 

### Rendu
- Fichier de configuration du pipeline
- Résultat d'un GET sur l'un des index 

## Exercice 2 (3 points)
Injection de données depuis un fichier dans Elasticsearch via logstash

### Consigne
1. Configurer Logstash (dans le même fichier) pour injecter les données depuis des fichiers JSON dans un nouvel index 

### Rendu
- Fichier de configuration du pipeline
- Résultat d'un GET sur l'un des index en filtrant sur `fields.name`

## Exercice 3 (2 points)
Tunning de Logstash

### Consigne
1. Découpé les 2 exercices précédent dans 2 fichiers de pipeline

### Rendu
- Les 2 fichiers de configuration de pipeline

## Exercice 4 (4 points)
De la belle DataViz 

### Consigne 
1. Ajouter 2 visualisations pour l'exercice 1
    - Un indicateur du nombre d'utilisateur injecté
    - Un graphique à barre stacké en fonction de l'age et du genre  
2. Ajouter 2 visualisations de votre choix pour l'exercice 2

### Rendu
- Pour chaque visualisation:
    - Une capture du rendu 
    - Une capture de la configuration
